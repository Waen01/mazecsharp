﻿using System;
using MazeBankCSHARP_Classes;
using UnityEngine;

public class MazeManager : MonoBehaviour
{

	public GameObject CellGameObject;
	public GameObject CaracterGameObject;
	public Material TransparentMaterial;
	public GameObject EndCup;
//	public GameObject EndCup;
	
	void Start ()
	{
		Maze maze = new Maze(10);
		
		for (int x = 0; x < maze.Size; x++)
		{
			for (int y = 0; y < maze.Size; y++)
			{
				GameObject obj =Instantiate(CellGameObject, new Vector3(x, 0, -y), Quaternion.identity);
				CellManager cell = obj.GetComponent<CellManager>();
				
				cell.SetWall(maze.CellTable[x,y]);

				if (y == 0)
				{
					cell.TopWall.GetComponent<Renderer>().material = TransparentMaterial;
				} else if (y == maze.Size - 1)
				{
					cell.BottomWall.GetComponent<Renderer>().material = TransparentMaterial;
				}
				if (x == 0)
				{
					cell.LeftWall.GetComponent<Renderer>().material = TransparentMaterial;
				} else if (x == maze.Size -1 )
				{
					cell.RightWall.GetComponent<Renderer>().material = TransparentMaterial;
				}

				if (!maze.CellTable[x, y].IsInPath)
				{
					cell.Ground.GetComponent<Renderer>().material = TransparentMaterial;
				}
			}
		}

		Instantiate(CaracterGameObject, new Vector3(maze.StartCell.Coord.X + 0.55f, 0.55f, -maze.StartCell.Coord.Y + 0.55f),
			Quaternion.identity);

		Instantiate(EndCup, new Vector3(maze.EndCell.Coord.X + 0.55f, 0.55F, -maze.EndCell.Coord.Y + 0.55f),
			Quaternion.identity);
	}
}
